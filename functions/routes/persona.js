'use strict';

var express = require('express');
var router  = express.Router();
var controller = require('../controllers/persona');
// var authorize = require('../middleware/jwt/auth');

router.post('/:manager', controller.managePersona);
module.exports = router;