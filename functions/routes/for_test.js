'use strict';

var express = require('express');
var router  = express.Router();
var controller = require('../controllers/for_test');
// var authorize = require('../middleware/jwt/auth');

router.post('/:manager', controller.testing);

module.exports = router;