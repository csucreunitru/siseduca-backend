'use strict';

var express = require('express');
var router  = express.Router();
var controller = require('../controllers/personal');
// var authorize = require('../middleware/jwt/auth');

router.post('/:manager', controller.managePersonal);
module.exports = router;