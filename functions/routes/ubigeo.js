'use strict';

var express = require('express');
var router  = express.Router();
var controller = require('../controllers/ubigeo');
// var authorize = require('../middleware/jwt/auth');

router.post('/:manager', controller.manageUbigeo);
module.exports = router;