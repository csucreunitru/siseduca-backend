'use strict';

var express = require('express');
var router  = express.Router();
var controller = require('../controllers/tipo_personal');
// var authorize = require('../middleware/jwt/auth');

router.post('/:manager', controller.manageTipoPersonal);
module.exports = router;