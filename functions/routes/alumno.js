'use strict';

var express = require('express');
var router  = express.Router();
var controller = require('../controllers/alumno');
// var authorize = require('../middleware/jwt/auth');

router.post('/:manager', controller.manageAlumno);
module.exports = router;