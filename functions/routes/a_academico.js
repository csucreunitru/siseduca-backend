'use strict';

var express = require('express');
var router  = express.Router();
var controller = require('../controllers/a_academico');
// var authorize = require('../middleware/jwt/auth');

router.post('/:manager', controller.manageAñoAcademico);
module.exports = router;