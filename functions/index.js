const functions = require('firebase-functions');
const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const cors = require('cors');
app.use(cors());
app.use(bodyParser.json()); // se usa para obtener los objetos que recibimos como formato json dentro de las variables req (en llamadas POST,PUT)  y se puede ver con req.body
app.use(bodyParser.urlencoded({ extended: true })); //no entendi pero parace importante xd

var models = require('./models');
//rutas

var ubigeo = require('./routes/ubigeo');
var persona = require('./routes/persona');
var testing = require('./routes/for_test');
var alumno = require('./routes/alumno');
var año_academico = require('./routes/a_academico');
var personal = require('./routes/personal');
var tipo_personal = require('./routes/tipo_personal');


app.use('/ubigeo',ubigeo);
app.use('/persona',persona);
app.use('/test',testing);

app.use('/alumno',alumno);
app.use('/a_academico',año_academico);
app.use('/personal',personal);
app.use('/tipo_personal',tipo_personal);

exports.app = functions.https.onRequest(app);
