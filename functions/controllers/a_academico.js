'use strict';
var models = require('../models');


exports.manageAñoAcademico = async(req,res)=>{
    var manager = req.params.manager;
    
    if(manager === 'listar_año_academico') {
        return getAñoAcademico(req,res);
    }else {
        return res.status(404).send("option not found");
    }
};


var getAñoAcademico = async (req,res)=>{
    await models.año_academico.findAll({
        where:{
            estado:'1',
        }
      }).then((ubigeos)=>{
          return res.json(ubigeos);
      }).catch((err)=>{
          return res.json(err);
      });
}
