'use strict';

//require('dotenv').config();

var models = require('../models');


exports.manageUbigeo = async(req,res)=>{
    var manager = req.params.manager;
    
    if (manager === 'traer_ubigeos') {
        return getUbigeos(req,res);
    }else{
        return res.status(404).send("option not found");
    }
};

var getUbigeos = async (req,res)=>{
    await models.ubigeo.findAll({
        where:{
            estado:'1',
        }
      }).then((ubigeos)=>{
          return res.json(ubigeos);
      }).catch((err)=>{
          return res.json(err);
      });
}