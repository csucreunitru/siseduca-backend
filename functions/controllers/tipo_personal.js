
var models = require('../models');


exports.manageTipoPersonal = async(req,res)=>{
    var manager = req.params.manager;
    
    if(manager === 'registrar_tipo_personal'){
        return registerTipoPersonal(req,res);
    }else if(manager === 'listar_tipo_personal') {
        return getTipoPersonal(req,res);
    }else if (manager === 'actualizar_tipo_personal') {
        return updateTipoPersonal(req,res);
    }else if (manager === 'eliminar_tipo_personal') {
        return deleteTipoPersonal(req,res);
    } else {
        return res.status(404).send("option not found");
    }
};


var getTipoPersonal = async (req,res)=>{
    await models.tipo_personal.findAll({
        where:{
            estado:'1',
        }
      }).then((ubigeos)=>{
          return res.json(ubigeos);
      }).catch((err)=>{
          return res.json(err);
      });
}
