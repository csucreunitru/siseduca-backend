'use strict';
var models = require('../models');


exports.manageAlumno = async(req,res)=>{
    var manager = req.params.manager;
    
    if(manager === 'registrar_alumno'){
        return registerAlumno(req,res);
    }else if(manager === 'listar_alumnos') {
        return getAlumnos(req,res);
    }else if (manager === 'actualizar_alumno') {
        return updateAlumno(req,res);
    }else if(manager === 'obtener_alumno_completo') {
        return retrieveFullAlumno(req,res);
    }else if (manager === 'eliminar_alumno') {
        return deleteAlumno(req,res);
    } else {
        return res.status(404).send("option not found");
    }
};

var registerAlumno = async (req,res)=>{
    let body = req.body;
    let persona_id = 0;
    let data_persona = {
        nombres: body.nombres,
        a_paterno: body.a_paterno,
        a_materno: body.a_materno,
        dni: body.dni,
        fecha_nacimiento: body.fecha,
        edad: body.edad,
        direccion: body.direccion,
        ubigeo_id: body.ubigeo_id,
    }
    let data_usuario = {
        login: body.login,
        pass: body.pass,
    }
    let data_alumno={
        año_academico_id: body.año_academico_id
    }
    
    
    await models.sequelize.transaction( async (t)=>{
        let personaObject = await models.persona.requestCreate(t,data_persona)
        if (personaObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        persona_id = personaObject.persona_id
        data_usuario.persona_id = persona_id
        let usuarioObject = await models.usuario.requestCreate(t,data_usuario)
        if (usuarioObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        
        data_alumno.persona_id = persona_id
        let alumnoObject = await models.alumno.requestCreate(t,data_alumno)
        if (alumnoObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        return res.status(200).json({respuesta: "Creacion exitosa",controlador: "alumno",operacion:"registerAlumno",registro:alumnoObject});
    });
};

var getAlumnos = async (req,res)=>{
    await models.alumno.findAll({
        where:{
            estado:'1',
        }
      }).then((persona)=>{
          return res.status(200).json(persona);
      }).catch((err)=>{
          return res.status(500).json(err);
      });
}

var updateAlumno = async (req,res)=>{
    let body = req.body;
    let _alumno_id = body.alumno_id;
    let data_persona = {
        nombres: body.nombres,
        a_paterno: body.a_paterno,
        a_materno: body.a_materno,
        dni: body.dni,
        fecha_nacimiento: body.fecha,
        edad: body.edad,
        direccion: body.direccion,
        ubigeo_id: body.ubigeo_id,
    }
    let data_alumno = {}
    if (body.hasOwnProperty("año_academico_id")) {data_alumno.año_academico_id = body.año_academico_id}
    
    await models.sequelize.transaction( async (t)=>{
        let alumnoObject = await models.alumno.requestFindById(t,_alumno_id)
        if (alumnoObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let persona_id = alumnoObject.persona_id
        
        let personaObject = await models.persona.requestFindById(t,persona_id)
        if (personaObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let personaUpdate = await models.persona.requestUpdate(t,personaObject,data_persona)
        if (personaUpdate.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }

        if(Object.keys(data_alumno).length > 0){
            let alumnoUpdate = await models.alumno.requestUpdate(t,alumnoObject,data_alumno)
            if (alumnoUpdate.hasOwnProperty('error_id')){
                return res.status(500).json("error ha implementar");
            }
        }
        return res.status(200).json({respuesta: "Actualizacion exitosa",controlador: "alumno",operacion:"updateAlumno",registro:alumnoObject});
    });
};

var deleteAlumno = async (req,res)=>{
    let body = req.body;
    //let usuario_id = 0;
    //let _persona_id = body.persona_id;
    //let _usuario_id = body.usuario_id;
    let _alumno_id = body.alumno_id;
    await models.sequelize.transaction( async (t)=>{
        let alumnoObject = await models.alumno.requestFindById(t,_alumno_id)
        if (alumnoObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        
        let alumnoUpdate = await models.alumno.requestDelete(t,alumnoObject)
        if (alumnoUpdate.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        
        let _persona_id = alumnoObject.persona_id
        let personaObject = await models.persona.requestFindById(t,_persona_id)
        if (personaObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let personaUpdate = await models.persona.requestDelete(t,personaObject)
        if (personaUpdate.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let usuarioObject = await models.usuario.requestFindOne(t,{persona_id:_persona_id})
        if (usuarioObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let usuarioUpdate = await models.usuario.requestDelete(t,usuarioObject)
        if (usuarioUpdate.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        return res.status(200).json({respuesta: "Eliminacion exitosa",controlador: "alumno",operacion:"deleteAlumno",registro:alumnoUpdate});
    });
};

var retrieveFullAlumno = async (req,res)=>{
    let body = req.body;
    let _alumno_id = body.alumno_id;
    await models.sequelize.transaction( async (t)=>{
        let alumnoObject = await models.alumno.requestFindById(t,_alumno_id)
        if (alumnoObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let _persona_id = alumnoObject.persona_id
        let personaObject = await models.persona.requestFindById(t,_persona_id)
        if (personaObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let usuarioObject = await models.usuario.requestFindOne(t,{persona_id:_persona_id})
        if (usuarioObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let FullData = {alumno:alumnoObject,persona: personaObject,usuario:usuarioObject};
        //let FullData = {...personalObject, ...personaObject,...usuarioObject};
        return res.status(200).json(FullData);
    });
}