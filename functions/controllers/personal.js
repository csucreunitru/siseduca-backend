'use strict';
var models = require('../models');


exports.managePersonal = async(req,res)=>{
    var manager = req.params.manager;
    
    if(manager === 'registrar_personal'){
        return registerPersonal(req,res);
    }else if(manager === 'listar_personal') {
        return getPersonal(req,res);
    }else if (manager === 'actualizar_personal') {
        return updatePersonal(req,res);
    }else if(manager === 'obtener_personal_completo') {
        return retrieveFullPersonal(req,res);
    }else if (manager === 'eliminar_personal') {
        return deletePersonal(req,res);
    } else {
        return res.status(404).send("option not found");
    }
};

var registerPersonal = async (req,res)=>{
    let body = req.body;
    //let usuario_id = 0;
    let persona_id = 0;
    //let año_academico_id = 0;
    let data_persona = {
        nombres: body.nombres,
        a_paterno: body.a_paterno,
        a_materno: body.a_materno,
        dni: body.dni,
        fecha_nacimiento: body.fecha,
        edad: body.edad,
        direccion: body.direccion,
        ubigeo_id: body.ubigeo_id,
    }
    let data_usuario = {
        login: body.login,
        pass: body.pass,
    }
    let data_personal={
        tipo_personal_id: body.tipo_personal_id
    }
    
    
    await models.sequelize.transaction( async (t)=>{
        let personaObject = await models.persona.requestCreate(t,data_persona)
        if (personaObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        data_usuario.persona_id = personaObject.persona_id
        //data_usuario.persona_id = persona_id
        let usuarioObject = await models.usuario.requestCreate(t,data_usuario)
        if (usuarioObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        
        data_personal.persona_id = data_usuario.persona_id
        let personalObject = await models.personal.requestCreate(t,data_personal)
        if (personalObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        return res.status(200).json({respuesta: "Creacion exitosa",controlador: "personal",operacion:"registerPersonal",registro:personalObject});
    });
};

var getPersonal = async (req,res)=>{
    await models.personal.findAll({
        where:{
            estado:'1',
        }
      }).then((persona)=>{
          return res.status(200).json(persona);
      }).catch((err)=>{
          return res.status(500).json(err);
      });
}

var updatePersonal = async (req,res)=>{
    let body = req.body;
    let _personal_id = body.personal_id;
    //let _persona_id = body.persona_id;
    let data_persona = {
        nombres: body.nombres,
        a_paterno: body.a_paterno,
        a_materno: body.a_materno,
        dni: body.dni,
        fecha_nacimiento: body.fecha,
        edad: body.edad,
        direccion: body.direccion,
        ubigeo_id: body.ubigeo_id,
    }
    let data_personal = {}
    if (body.hasOwnProperty("tipo_personal_id")) {data_personal.tipo_personal_id = body.tipo_personal_id}
    
    await models.sequelize.transaction( async (t)=>{
        let personalObject = await models.personal.requestFindById(t,_personal_id)
        if (personalObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let persona_id = personalObject.persona_id
        
        let personaObject = await models.persona.requestFindById(t,persona_id)
        if (personaObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let personaUpdate = await models.persona.requestUpdate(t,personaObject,data_persona)
        if (personaUpdate.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }

        if(Object.keys(data_personal).length > 0){
            let personalUpdate = await models.personal.requestUpdate(t,personalObject,data_personal)
            if (personalUpdate.hasOwnProperty('error_id')){
                return res.status(500).json("error ha implementar");
            }
        }
        return res.status(200).json({respuesta: "Actualizacion exitosa",controlador: "personal",operacion:"updatePersonal",registro:personalObject});
    });
};

var deletePersonal = async (req,res)=>{
    let body = req.body;
    //let usuario_id = 0;
    //let _persona_id = body.persona_id;
    //let _usuario_id = body.usuario_id;
    let _personal_id = body.personal_id;
    await models.sequelize.transaction( async (t)=>{
        let personalObject = await models.personal.requestFindById(t,_personal_id)
        if (personalObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let personalUpdate = await models.personal.requestDelete(t,personalObject)
        if (personalUpdate.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let _persona_id = personalObject.persona_id
        let personaObject = await models.persona.requestFindById(t,_persona_id)
        if (personaObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let personaUpdate = await models.persona.requestDelete(t,personaObject)
        if (personaUpdate.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let usuarioObject = await models.usuario.requestFindOne(t,{persona_id:_persona_id})
        if (usuarioObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let usuarioUpdate = await models.usuario.requestDelete(t,usuarioObject)
        if (usuarioUpdate.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        return res.status(200).json({respuesta: "Eliminacion exitosa",controlador: "personal",operacion:"deletePersonal",registro:personalObject});
    });
};

var retrieveFullPersonal = async (req,res)=>{
    let body = req.body;
    //let usuario_id = 0;
    //let _persona_id = body.persona_id;
    //let _usuario_id = body.usuario_id;
    let _personal_id = body.personal_id;
    await models.sequelize.transaction( async (t)=>{
        let personalObject = await models.personal.requestFindById(t,_personal_id)
        if (personalObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let _persona_id = personalObject.persona_id
        let personaObject = await models.persona.requestFindById(t,_persona_id)
        if (personaObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let usuarioObject = await models.usuario.requestFindOne(t,{persona_id:_persona_id})
        if (usuarioObject.hasOwnProperty('error_id')){
            return res.status(500).json("error ha implementar");
        }
        let FullData = {personal:personalObject,persona: personaObject,usuario:usuarioObject};
        //let FullData = {...personalObject, ...personaObject,...usuarioObject};
        return res.status(200).json(FullData);
    });
}