const path = require('path');
const error_list = require(path.join('..','config','errors.json'));

class error_sender{
    constructor(res,error_data) {
        this.res = res;
        this.error_id = error_data["error_id"];
        this.error_data = error_data;
    }
    /*
        Funcion para buscar el error por su identificador dentro del json de errores
    */
    get_error_by_id(error_id){
        if(error_list.hasOwnProperty(error_id)){
            return error_list[error_id];
        }
        return error_list["invalid_error_code"];
    }

    make_error_response(){
        var error_data = this.get_error_by_id(this.error_id);
        var message = this.fill_error_message(error_data["message"],this.error_data);
        return this.res.status(error_data["status"]).json(message);
    }

    /* 
    Funcion para construir el mensaje de error devuelto por backend
    
    RegExp: constructor de expresiones regulares, la segunda parte son los modificadores de expresion regular
        g: global -- encaso encontrar repeticiones, tambien las va a considerar
        i: insensitivo -- desactiva el reconocimiento entre mayusculas y minusculas
    */
    fill_error_message(message,error_data){
        let string_to_regex = "default"
        let regex = new RegExp(string_to_regex, "gi");
        error_data.forEach(llave => {
            string_to_regex = "%"+llave+"%";
            regex = new RegExp(string_to_regex, "gi");
            message = message.replace(regex,error_data[llave])
        });
        return message
    }
}
    
module.exports = error_sender;