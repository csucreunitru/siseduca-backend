'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('tipo_personal', {
      tipo_personal_id: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      nombre:{
        allowNull:false,
        type: Sequelize.STRING(50),
        defaultValue:" "
      },
      descripcion: {
        allowNull:false,
        type: Sequelize.STRING(200),
        defaultValue:" "
      },
      estado: {
        allowNull:false,
        type: Sequelize.CHAR(1),
        defaultValue:'1'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('tipo_personal');
     */
    await queryInterface.dropTable('tipo_personal');
  }
};
