'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ubigeo', {
      ubigeo_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      departamento: {
        allowNull:false,
        type: Sequelize.STRING(50)
      },
      provincia: {
        allowNull:false,
        type: Sequelize.STRING(50)
      },
      ubigeo: {
        allowNull:false,
        type: Sequelize.STRING(50),
        unique:true
      },
      distrito: {
        allowNull:false,
        type: Sequelize.STRING(50)
      },
      estado: {
        allowNull:false,
        type: Sequelize.CHAR(1),
        defaultValue:'1'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ubigeo');
  }
};