'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('usuario', {
      usuario_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      login: {
        allowNull:false,
        type: Sequelize.STRING(50)
      },
      pass: {
        allowNull:false,
        type: Sequelize.STRING
      },
      estado: {
        allowNull:false,
        type: Sequelize.CHAR(1),
        defaultValue:'1'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      persona_id:{
        type: Sequelize.INTEGER,
        references:{
          model:'persona',
          key:'persona_id',
        },
        onUpdate:'CASCADE',
        onDelete:'SET NULL'
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('usuario');
  }
};
