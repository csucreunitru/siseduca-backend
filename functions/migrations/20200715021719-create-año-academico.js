'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('año_academico', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('año_academico', {
      año_academico_id: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      año: {
        type:Sequelize.DATE,
        allowNull:false
      },
      estado: {
        allowNull:false,
        type: Sequelize.CHAR(1),
        defaultValue:'1'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('año_academico');
     */
    await queryInterface.dropTable('año_academico');
  }
};
