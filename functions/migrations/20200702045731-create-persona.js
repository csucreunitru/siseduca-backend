'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('persona', {
      persona_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombres:{
        allowNull:false,
        type: Sequelize.STRING(100)
      },
      a_paterno: {
        allowNull:false,
        type: Sequelize.STRING(50)
      },
      a_materno: {
        allowNull:false,
        type: Sequelize.STRING(50)
      },
      dni: {
        allowNull:false,
        type: Sequelize.STRING(15)
      },
      fecha_nacimiento: {
        allowNull:false,
        type: Sequelize.DATE
      },
      edad: {
        allowNull:false,
        type: Sequelize.INTEGER
      },
      direccion: {
        allowNull:false,
        type: Sequelize.STRING(200)
      },
      estado: {
        allowNull:false,
        type: Sequelize.CHAR(1),
        defaultValue:'1'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      ubigeo_id:{
        type: Sequelize.INTEGER,
        references:{
          model:'ubigeo',
          key:'ubigeo_id',
        },
        onUpdate:'CASCADE',
        onDelete:'SET NULL'
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('persona');
  }
};
