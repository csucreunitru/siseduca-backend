'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('personal', {
      personal_id: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      estado: {
        allowNull:false,
        type: Sequelize.CHAR(1),
        defaultValue:'1'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      persona_id:{
        type: Sequelize.INTEGER,
        references:{
          model:'persona',
          key:'persona_id',
        },
        onUpdate:'CASCADE',
        onDelete:'SET NULL'
      },
      tipo_personal_id:{
        type: Sequelize.INTEGER,
        references:{
          model:'tipo_personal',
          key:'tipo_personal_id',
        },
        onUpdate:'CASCADE',
        onDelete:'SET NULL'
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('personal');
     */
    await queryInterface.dropTable('personal');
  }
};

