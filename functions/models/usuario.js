'use strict';
const {
  Model
} = require('sequelize');
const persona = require('./persona');
module.exports = (sequelize, DataTypes) => {
  class usuario extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.persona,{foreignKey:'persona_id'});
    }

    static async requestFindById(t,id){
      return usuario.findOne({
          where:{
              usuario_id: id
          }
      },{transaction: t}).then((usuario)=>{
        if (!usuario) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"usuario",
            "id":String(id)
          }
        }
        return usuario
      });
    };
    static async requestFindOne(t,where){
      return usuario.findOne({
          where:where
      },{transaction: t}).then((usuario)=>{
        if (!usuario) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"usuario",
            "id":"undefined"
          }
        }
        return usuario
      });
    };
    static async requestUpdate(t,usuarioUpdate,data){
        return usuarioUpdate.update(
            data
        ,{transaction:t}).then((usuario)=>{
          if (!usuario) {
              errores="No se actualizó usuario";
              t.rollback();
              return  {
                "error_id":"record_not_updated",
                "model":"usuario",
                "id": String(usuarioUpdate.id)
              }
              //res.status(500).json(errores);
          }
          return usuarioUpdate;
        });
    };
    static async requestCreate(t,data){
        return usuario.create(
          data
        ,{transaction: t}).then((usuario)=>{
              if (!usuario) {
                  t.rollback();
                  return {
                  "error_id":"record_not_created",
                  "model": "usuario"
                }
              }
              return usuario
        });
    };
    static async requestDelete(t,usuarioDelete){
        return usuarioDelete.update({
            estado:"0",
        },{transaction:t}).then((usuario)=>{
          if (!usuario) {
              //errores="No se eliminó. ";
              t.rollback();
              return {
                "error_id":"record_not_deleted",
                "model":"usuario",
                "id":String(usuarioDelete.usuario_id)
              }
          }
          return true
        });
    };
  }
  usuario.init({
    usuario_id: {
      type:DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    login: {
      allowNull:false,
      type: DataTypes.STRING(50)
    },
    pass: {
      allowNull:false,
      type: DataTypes.STRING(32)
    },
    persona_id:{
      type:DataTypes.INTEGER,
      allowNull:false,
    },
    estado: {
      allowNull:false,
      type: DataTypes.BOOLEAN,
      defaultValue:'1'
    },
  }, {
    sequelize,
    tableName: 'usuario',
    modelName: 'usuario',
    timestamps: true,
  });
  return usuario;
};