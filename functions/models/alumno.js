'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class alumno extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.persona,{foreignKey:'persona_id'});
      this.belongsTo(models.año_academico,{foreignKey:'año_academico_id'});
    }
    static async requestFindById(t,id){
      return alumno.findOne({
          where:{
              alumno_id: id
          }
      },{transaction: t}).then((alumno)=>{
        if (!alumno) {
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"alumno",
            "id":String(alumno.alumno_id)
          }
        }
        return alumno
      });
    };
    static async requestFindOne(t,where,fields){
      return alumno.findOne({
          where:where
      },{transaction: t}).then((alumno)=>{
        if (!alumno) {
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"alumno",
            "id":"undefined"
          }
        }
        return alumno
      });
    };
    static async requestUpdate(t,alumnoUpdate,data){
        return alumnoUpdate.update(
            data
        ,{transaction:t}).then((alumno)=>{
          if (!alumno) {
              errores="No se actualizó persona";
          }
          return alumno;
        }).catch((error) => {
          return  {
            "error_id":"record_not_updated",
            "model":"alumno",
            "id": String(alumnoUpdate.id),
            "error":String(error)
          }
        });
    };
    static async requestCreate(t,data){
        return alumno.create(
          data
        ,{transaction: t}).then((alumno)=>{
              if (!alumno) {
                t.rollback();
              }
              return alumno
        }).catch((error) => {
          return {
            "error_id":"record_not_created",
            "model":"alumno",
            "error":String(error)
          }
        });
    };
    static async requestDelete(t,alumnoDelete){
        return alumnoDelete.update({
            estado:"0",
        },{transaction:t}).then((alumno)=>{
          if (!alumno) {
              t.rollback();
          }
          return true
        }).catch((error) => {
          return {
            "error_id":"record_not_deleted",
            "model":"alumno",
            "id":String(alumnoDelete.id),
            "error":String(error)
          }
        });
    };
  }
  alumno.init({
    alumno_id: {
        type:DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    estado: {
      allowNull:false,
      type: DataTypes.BOOLEAN,
      defaultValue:'1'
    },
    persona_id: {
      type:DataTypes.INTEGER,
      allowNull:false,
    },
    año_academico_id: {
      type:DataTypes.INTEGER,
      allowNull:false,
    }
  }, {
    sequelize,
    tableName: 'alumno',
    modelName: 'alumno',
    timestamps: true,
  });
  return alumno;
};