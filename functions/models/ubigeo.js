'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ubigeo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.persona,{foreignKey:'ubigeo_id'});
    }

    static async requestFindById(t,id){
      return ubigeo.findOne({
          where:{
              ubigeo_id: id
          }
      },{transaction: t}).then((ubigeo)=>{
        if (!ubigeo) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"usuario",
            "id":String(id)
          }
        }
        return ubigeo
      });
    };
    static async requestFindOne(t,where){
      return ubigeo.findOne({
          where:where
      },{transaction: t}).then((ubigeo)=>{
        if (!ubigeo) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"ubigeo",
            "id":"undefined"
          }
        }
        return ubigeo
      });
    };
    static async requestUpdate(t,ubigeoUpdate,data){
        return ubigeoUpdate.update(
            data
        ,{transaction:t}).then((ubigeo)=>{
          if (!ubigeo) {
              errores="No se actualizó usuario";
              t.rollback();
              return  {
                "error_id":"record_not_updated",
                "model":"usuario",
                "id": String(ubigeoUpdate.id)
              }
              //res.status(500).json(errores);
          }
          return ubigeo;
        });
    };
    static async requestCreate(t,data){
        return ubigeo.create(
          data
        ,{transaction: t}).then((ubigeo)=>{
              if (!ubigeo) {
                  t.rollback();
                  return {
                  "error_id":"record_not_created",
                  "model": "ubigeo"
                }
              }
              return ubigeo
        });
    };
    static async requestDelete(t,ubigeoDelete){
        return ubigeoDelete.update({
            estado:"0",
        },{transaction:t}).then((usuario)=>{
          if (!usuario) {
              //errores="No se eliminó. ";
              t.rollback();
              return {
                "error_id":"record_not_deleted",
                "model":"usuario",
                "id":String(usuarioDelete.usuario_id)
              }
          }
          return true
        });
    };
    
  }
  ubigeo.init({
    ubigeo_id: {
      type:DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    departamento: {
      allowNull:false,
      type: DataTypes.STRING(50)
    },
    provincia: {
      allowNull:false,
      type: DataTypes.STRING(50)
    },
    ubigeo: {
      allowNull:false,
      type: DataTypes.STRING(50),
      unique:true
    },
    distrito: {
      allowNull:false,
      type: DataTypes.STRING(50)
    },
    estado: {
      type: DataTypes.CHAR(1),
      defaultValue:'1'
    }
  }, {
    sequelize,
    tableName: 'ubigeo',
    modelName: 'ubigeo',
    timestamps: true,
  });
  return ubigeo;
};