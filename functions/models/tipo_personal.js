'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tipo_personal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
    }

    static async requestFindById(t,id){
      return tipo_personal.findOne({
          where:{
            tipo_personal_id: id
          }
      },{transaction: t}).then((tipo_personal)=>{
        if (!tipo_personal) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"tipo_personal",
            "id":String(id)
          }
        }
        return tipo_personal
      });
    };
    static async requestFindOne(t,where){
      return tipo_personal.findOne({
          where:where
      },{transaction: t}).then((tipo_personal)=>{
        if (!tipo_personal) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"tipo_personal",
            "id":"undefined"
          }
        }
        return tipo_personal
      });
    };
    static async requestUpdate(t,tipo_personalUpdate,data){
        return tipo_personalUpdate.update(
            data
        ,{transaction:t}).then((tipo_personal)=>{
          if (!tipo_personal) {
              errores="No se actualizó persona";
              t.rollback();
              return  {
                "error_id":"record_not_updated",
                "model":"tipo_personal",
                "id": String(tipo_personalUpdate.id)
              }
              //res.status(500).json(errores);
          }
          return tipo_personalUpdate;
        });
    };
    static async requestCreate(t,data){
        return tipo_personal.create(
        data
        ,{transaction: t}).then((tipo_personal)=>{
            if (!tipo_personal) {
                //errores = errores + "Error al crear persona.";
                t.rollback();
                return {
                  "error_id":"record_not_created",
                  "model": "tipo_personal"
                }
            }
            return tipo_personal
        });
    };
    static async requestDelete(t,tipo_personalDelete){
        return tipo_personalDelete.update({
            estado:"0",
        },{transaction:t}).then((tipo_personal)=>{
          if (!tipo_personal) {
              //errores="No se eliminó. ";
              t.rollback();
              return {
                "error_id":"record_not_deleted",
                "model":"tipo_personal",
                "id":String(id)
              }
          }
          return true
        });
    };
  }
  tipo_personal.init({
    tipo_personal_id: {
      type:DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      allowNull:false,
      type: DataTypes.STRING(100)
    },
    descripcion: {
      allowNull:false,
      type: DataTypes.STRING(50)
    },
    estado: {
      allowNull:false,
      type: DataTypes.BOOLEAN,
      defaultValue:'1'
    }
  }, {
    sequelize,
    tableName: 'tipo_personal',
    modelName: 'tipo_personal',
    timestamps: true,
  });
  return tipo_personal;
};