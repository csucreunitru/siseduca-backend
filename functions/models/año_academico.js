'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class año_academico extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
    }

    static async requestFindById(t,id){
      return año_academico.findOne({
          where:{
              año_academico_id: id
          }
      },{transaction: t}).then((año_academico)=>{
        if (!año_academico) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"año_academico",
            "id":String(id)
          }
        }
        return año_academico
      });
    };
    static async requestFindOne(t,where){
      return año_academico.findOne({
          where:where
      },{transaction: t}).then((año_academico)=>{
        if (!año_academico) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"año_academico",
            "id":"undefined"
          }
        }
        return año_academico
      });
    };
    static async requestUpdate(t,año_academicoUpdate,data){
        return año_academicoUpdate.update(
            data
        ,{transaction:t}).then((año_academico)=>{
          if (!año_academico) {
              errores="No se actualizó año_academico";
              t.rollback();
              return  {
                "error_id":"record_not_updated",
                "model":"año_academico",
                "id": String(año_academicoUpdate.año_academico_id)
              }
              //res.status(500).json(errores);
          }
          return año_academico;
        });
    };
    static async requestCreate(t,data){
        return año_academico.create(
          data
        ,{transaction: t}).then((año_academico)=>{
              if (!año_academico) {
                  t.rollback();
                  return {
                  "error_id":"record_not_created",
                  "model": "año_academico"
                }
              }
              return año_academico
        });
    };
    static async requestDelete(t,año_academicoDelete){
        return año_academicoDelete.update({
            estado:"0",
        },{transaction:t}).then((año_academico)=>{
          if (!año_academico) {
              //errores="No se eliminó. ";
              t.rollback();
              return {
                "error_id":"record_not_deleted",
                "model":"año_academico",
                "id":String(año_academicoDelete.año_academico_id)
              }
          }
          return true
        });
    };
  }
  año_academico.init({
    año_academico_id: {
      type:DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    año:{
        type:DataTypes.INTEGER,
        allowNull:false,
    },
    estado: {
      allowNull:false,
      type: DataTypes.BOOLEAN,
      defaultValue:'1'
    }
  }, {
    sequelize,
    tableName: 'año_academico',
    modelName: 'año_academico',
    timestamps: true,
  });
  return año_academico;
};