'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class personal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      //this.hasOne(models.persona,{foreignKey:'persona_id'});
      this.belongsTo(models.persona,{foreignKey:'persona_id'});
      this.belongsTo(models.tipo_personal,{foreignKey:'tipo_personal_id'});
    };

    static async requestFindById(t,id){
        return personal.findOne({
            where:{
                personal_id: id
            }
        },{transaction: t}).then((personal)=>{
          if (!personal) {
            //errores= "No se encontro personal. ";
            t.rollback();
            return {
              "error_id":"record_not_found",
              "model":"personal",
              "id":String(id)
            }
          }
          return personal
        });
    };
    static async requestFindOne(t,where){
      return personal.findOne({
          where:where
      },{transaction: t}).then((personal)=>{
        if (!personal) {
          //errores= "No se encontro personal. ";
          t.rollback();
          return {
            "error_id":"record_not_found",
            "model":"personal",
            "id":"undefined"
          }
        }
        return personal
      });
    };
    static async requestUpdate(t,personalUpdate,data){
        return personalUpdate.update(
            data
        ,{transaction:t}).then((personal)=>{
          if (!personal) {
              errores="No se actualizó persona";
              t.rollback();
              return  {
                "error_id":"record_not_updated",
                "model":"personal",
                "id": String(personalUpdate.id)
              }
              //res.status(500).json(errores);
          }
          return personalUpdate;
        });
    };
    static async requestCreate(t,data){
        return personal.create(
         data
        ,{transaction: t}).then((personal)=>{
             if (!personal) {
                 //errores = errores + "Error al crear persona.";
                 t.rollback();
                 return {
                  "error_id":"record_not_created",
                  "model": "personal"
                }
             }
             return personal
        });
    };
    static async requestDelete(t,personalDelete){
        return personalDelete.update({
            estado:"0",
        },{transaction:t}).then((personal)=>{
          if (!personal) {
              //errores="No se eliminó. ";
              t.rollback();
              return {
                "error_id":"record_not_deleted",
                "model":"personal",
                "id":String(personalDelete.id)
              }
          }
          return true
        });
    };
  }
  personal.init({
    personal_id: {
      type:DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    estado: {
      allowNull:false,
      type: DataTypes.BOOLEAN,
      defaultValue:'1'
    },
    persona_id: {
      type:DataTypes.INTEGER,
      allowNull:false,
    },
    tipo_personal_id: {
      type:DataTypes.INTEGER,
      allowNull:false,
    }
  }, {
    sequelize,
    tableName: 'personal',
    modelName: 'personal',
    timestamps: true,
  });

  return personal;
};